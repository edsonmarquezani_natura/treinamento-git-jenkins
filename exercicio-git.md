# Exercício Git

[**Link para apresentação**](https://naturabr-my.sharepoint.com/:p:/g/personal/edsonfilho_natura_net/EbxoGduw5GVGo0p7IHGnlpoBhOj2zzEjLWuzuOtaekKW-Q?e=msdyaA)

## Objetivo
Exercitar os conceitos básicos de _Git_ expostos no treinamento e explorar o fluxo básico de utilização da plataforma [**Bitbucket**](https://bitbucket.org).

## Passos

### Configuração global
- Configure os dados de autor do Git
```
git config --global user.email "<seuemail>"
git config --global user.name "<seunome>"
```

### Setup Bitbucket
- [Crie uma conta](https://bitbucket.org/account/signup/) no **_Bitbucket_** (caso ainda não possua).
- Crie um novo repositório e siga as instruções para fazer o setup inicial

### Operações

**Obs: Lembre-se de sempre fazer _commit_ das alterações e enviar para o servidor remoto (_Bitbucket_), após cada operação.**
```
git add [...]
git commit [...]
git push [...]
```

#### Pull Request
1. Crie um arquivo de nome qualquer com 5 linhas de texto (com qualquer conteúdo).
2. Crie uma nova ***branch***, e altere a linha 2 do arquivo.
3. Volte para a _branch_ _master_.
4. Altere a linha 4 do arquivo.
5. Pelo ***Bitbucket***, gere um ***Pull Request*** dessa _branch_ destinada à _master_.
6. Execute o ***merge*** via _Bitbucket_, pela interface.
7. No seu repositório local, atualize a ***branch master***, para refletir o merge recém-realizado no servidor.
8. Verifique o conteúdo do arquivo.

#### Conflito
1. Repita os passos do item anterior, porém, dessa vez, altere na nova _branch_ e na _master_ uma **mesma linha**.  **(Crie uma nova branch. Não reutilze a do item anterior.)**
2. Ao tentar fazer o merge do ***Pull Request***, observe que há um aviso de conflito. Resolva-o.

#### Acessar branch
1. Crie uma branch, dessa vez pelo _Bitbucket_.
2. Use o comando `git fetch` para atualizar as informações de repositório local a respeito das branchs remotas.
3. Troque para a branch recém-criada e faça um _commit_ qualquer.

#### Taggeando versão
1. Crie uma tag apontando para o antepenúltimo commit da branch _master_.
2. Envia a tag para o servidor com a opção `--tags` do comando `push`.
3. Verifique no _Bitbucket_ se a tag foi enviada com sucesso.

#### Criar branch de hotfix
1. Crie uma branch de nome `hotfix`, que deve nascer a partir da tag recém criada, e **não do HEAD da branch**.

#### Trocar de branch sem fazer _commit_
1. Verifique o comportamento do Git.
