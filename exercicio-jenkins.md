# Exercício Jenkins

## Objetivo
Criar um job de deploy no _Jenkins_, que a partir de uma lista de opções fornecidas ao usuário, baixe um template de página web e faça o _deploy_ como uma aplicação na _Heroku_.

Os templates serão obtidos dos temas oferecidos aqui: https://html5up.net/ .

URL Alternativo: http://natura-treinamento-jenkins.s3-website-us-east-1.amazonaws.com/`nome`/download

## Passos

### Heroku

#### Setup da Conta
- Crie uma conta free na [*Heroku*](https://signup.heroku.com/).
- Confirme a conta atrás do email enviado.
- Efetue log-in no painel.

#### Setup Application
- Canto superior direito da tela -> `New` -> `Create New App`
- Dê um nome à aplicação e mande criar (pode ser usada a região padrão - USA)

#### Obter informações para deploy
Obtenha os seguintes dados, a serem usados posteriormente durante a configuração do job do Jenkins.

1. Na tela de configuração da aplicação recém-criada, vá `Settings` -> `Info` -> `Heroku Git URL`. Anote a URL.
2. No canto superior direito, na foto do usário, vá em `Account Settings` -> `Api Key` -> `Reveal`. Anote a chave.

#### Setup de código
- Faça clone do repositório git da aplicação Heroku e adicione os seguintes arquivos, com o conteúdo indicado abaixo:

    - _Procfile_
        - `web: vendor/bin/heroku-php-apache2 .`
    - _index.php_
        - `<?php header( 'Location: /index.html' );?>`

**Obs: faça o clone pela URL obtida acima, com usuário `git` e usa como senha a API Key**

- Faça commit e empurre para o servidor

### Jenkins
#### Configuração
- Vá em `Manage Jenkins` -> `Configure Jenkins` -> `# of executors`, entre o valor `1`, e salve.

#### Credenciais
- Crie uma credencial do tipo _username/password_, com usuário `git` e no lugar da senha, o valor de _API Key_ obtido anteriormente.

#### Job
- Crie um job do tipo _Freestyle_ (`New Item` -> `Freestyle Job`)

- Configure o repositório Git da aplicação Heroku em `Source Code Management` -> `Git`. Indique a URL do repositório (obtida anteriormente) e a credencial recém-criada.

- Configure um parâmetro para o job em `General` -> `This project is parameterized` -> `Add Parameter` -> `Choice Parameter`.

    - Dê um nome ao parâmetro e em `Choices`, insira uma lista com os nomes dos temas disponíveis em https://html5up.net/. Exemplo:
```
massively
ethereal
editorial
forty
stellar
```

- Configure a etapa de build

    - Adicione um _build step_ (`Build` -> `Add build step`) do tipo `Execute shell`, e nele faça o download do _zip_ do tema (use o comando `wget`) e descompacte seu conteúdo (comando `unzip`).

    - Adiciona um segundo step também do tipo, que deve realizar _commit_ das alterações.

- Configure o deploy

    - Adicione um _post-build step_ (`Post-build Actions` -> `Add post-build action`) do tipo `Git Publisher`. Configure o step para fazer push das alterações na branch remota `master`.

- Rode o job e verifique se o deploy ocorre, acessando a URL web da aplicação do _Heroku_ (_https://`<nomedaapp>`.herokuapp.com_).
